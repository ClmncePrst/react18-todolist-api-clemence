import { useState } from "react";
import { useEffect } from "react";
import axios from 'axios';
import "./App.css";
import TodoList from "./components/TodoList";

// URL de l'API pour récupérer les todos
const getAPIURL = "https://jsonplaceholder.typicode.com/todos"

export default function App() {
  // stockage des todo reçus par l'API
  const [todos, setTodos] = useState([]);

  // TODO install and import axios, create an useEffect hook, and call the API

  // Utilisation du Hook UseEffect 
  useEffect(() => {
// Utilisation de la commande axios.get pour faire une requête HTTP
      axios.get(getAPIURL)
        .then(response => {
// Utilisation du state Todos
          setTodos(response.data);
        })
      }, []); // le tableau vide fait que l'effet ne s'exécute qu'une fois (auparavant affichait fonction récursive)

      return (
        <div className="App">
          <TodoList todos={todos} />
        </div>
      );
  }